#!/usr/bin/env python

import argparse
import logging
import sys

from google_sheets_emulator.server import create_server

# Random, apparently not often used
DEFAULT_PORT = 9077


def run_server(host, port, data_dir):
    server = create_server(host, port, data_dir)
    try:
        return server.run()
    except KeyboardInterrupt:
        pass


def prepare_args_parser():
    parser = argparse.ArgumentParser(description='Google Cloud Task Emulator')
    subparsers = parser.add_subparsers(title='subcommands', dest="subcommand")
    start = subparsers.add_parser('start', help='start the emulator')
    start.add_argument(
        "-p", "--port",
        type=int, help='the port to run the server on', default=DEFAULT_PORT
    )
    start.add_argument(
        "-d", "--data-dir",
        type=str, help="The folder to store spreadsheets",
        default=None,
    )
    start.add_argument("-q", "--quiet", action="store_true", default=False)

    return parser


def main():
    print("Starting Google Sheets Emulator")
    parser = prepare_args_parser()
    args = parser.parse_args()
    if args.subcommand != "start":
        parser.print_usage()
        sys.exit(1)
    root = logging.getLogger()
    root.addHandler(logging.StreamHandler())
    if args.quiet:
        root.setLevel(logging.ERROR)
    else:
        root.setLevel(logging.INFO)

    sys.exit(run_server("localhost", args.port, args.data_dir))


if __name__ == '__main__':
    main()
